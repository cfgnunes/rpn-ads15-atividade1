FROM nginx

ARG VARIAVEL="Cristiano Nunes"

COPY index.html /usr/share/nginx/html/

RUN sed -i "s|Hello World|Oi $VARIAVEL|g" /usr/share/nginx/html/index.html
